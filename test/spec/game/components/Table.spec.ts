import { Table } from '../../../../src/game/components'
import { expect } from 'chai'
import 'mocha'

describe('rankingTable points: maint, pr, bugfix', () => {
    var createTable = function() {
        let players = ['JohnConnor', 'PeterParker', 'JohnDoe']
        let config = {
            players: players,
            points: {
                maint: {
                    display: 'raw',
                    multipler: 10
                },
                bugfix: {
                    display: 'raw',
                    multipler: 7
                },
                pr: {
                    display: 'raw',
                    multipler: 3
                }
            }
        }
        let table = new Table(config)
        return table
    }    
    it('table result should have 3 rows', () => {
        let table = createTable()
        let result = table.getTable()
        expect(3).is.equal(result.length)
    })
    it('after add 1 pr point to JohnConnor he should be at first row result', () => {
        let table = createTable()
        table.addPoints('pr', 1, 'JohnConnor')
        let result = table.getTable()
        expect('JohnConnor').is.equal(result[0].playerId)
        expect(1).is.equal(result[0].points.pr)
    })
    it('after add 1 bugfix point to JohnConnor he should be at first row result', () => {
        let table = createTable()
        table.addPoints('bugfix', 1, 'JohnConnor')
        let result = table.getTable()
        expect('JohnConnor').is.equal(result[0].playerId)
        expect(1).is.equal(result[0].points.bugfix)
    })
    it('after add 1 maint point to JohnConnor he should be at first row result', () => {
        let table = createTable()
        table.addPoints('maint', 1, 'JohnConnor')
        let result = table.getTable()
        expect('JohnConnor').is.equal(result[0].playerId)
        expect(1).is.equal(result[0].points.maint)
    })
    it('after add 1 pr,bugfix and maint point to JohnConnor he should be at first row result and have 20 points', () => {
        let table = createTable()
        table.addPoints('pr', 1, 'JohnConnor')
        table.addPoints('bugfix', 1, 'JohnConnor')
        table.addPoints('maint', 1, 'JohnConnor')
        let result = table.getTable()
        expect('JohnConnor').is.equal(result[0].playerId)
        expect(20).is.equal(result[0].points.total)
    })
})