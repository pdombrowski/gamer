import { ChallengeBox } from '../../../../src/game/components'
import { expect } from 'chai'
import 'mocha'

var players = ['JohnConnor', 'PeterParker', 'EricEvans']

describe('challengeBox', () => {
    it('JohnConnor should have 2 points for pr, maint and bugfix and PeterParker should have 3 points for bugfix', () => {
        let challengeBox = new ChallengeBox({
            players: players,
            challengesConfig: [
                {
                    name: 'pr',
                    goals: [1, 3, 5, 7, 10]
                },
                {
                    name: 'maint',
                    goals: [1, 2, 3, 5, 8]
                },
                {
                    name: 'bugfix',
                    goals: [1, 2, 3, 4, 5, 100]
                }
            ]
        })
        challengeBox.addChallengePoints('JohnConnor', 'pr', 2)
        challengeBox.addChallengePoints('JohnConnor', 'maint', 2)
        challengeBox.addChallengePoints('JohnConnor', 'bugfix', 2)
        challengeBox.addChallengePoints('PeterParker', 'bugfix', 3)

        let rawChallenges = challengeBox.getRawChallenges()

        expect(2).is.equal(rawChallenges.JohnConnor.pr.points)
        expect(2).is.equal(rawChallenges.JohnConnor.maint.points)
        expect(2).is.equal(rawChallenges.JohnConnor.bugfix.points)
        expect(3).is.equal(rawChallenges.PeterParker.bugfix.points)
    })
})