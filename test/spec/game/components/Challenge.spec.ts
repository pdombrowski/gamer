import { Challenge } from '../../../../src/game/components'
import { expect } from 'chai'
import 'mocha'

describe('challenge', () => {
    it('challenge should return level 2 and next goal 6', () => {
        let challenge = new Challenge({
            name: 'challenge1',
            points: 4,
            goals: [3,4,6,8]
        })
        expect(3).is.equal(challenge.getLevel())
        expect(6).is.equal(challenge.getGoal())
    })
    it('challenge should return last level and last goal when overflowed', () => {
        let challenge = new Challenge({
            name: 'challenge1',
            points: 10,
            goals: [3,4,6,8]
        })
        expect(5).is.equal(challenge.getLevel())
        expect(8).is.equal(challenge.getGoal())
    })

    it('challenge should return level 1 and goal 3 when points is 0', () => {
        let challenge = new Challenge({
            name: 'challenge1',
            points: 0,
            goals: [3,4,6,8]
        })
        expect(1).is.equal(challenge.getLevel())
        expect(3).is.equal(challenge.getGoal())
    })
})
