import { WildcardMatcher } from '../../../src/WildcardMatcher'
import { expect } from 'chai'
import 'mocha'

describe('WildcardMatcher.isMatch truthy wildcards test', () => {
    it("should return true if wildcard is '#' and value is foo.bar.baz.bam", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('#', 'foo.bar.baz.bam'))
    })
    it("should return true if wildcard is '*' and value is foo", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*', 'foo'))
    })
    it("should return true if wildcard is 'foo.*.baz' and value is foo.bar.baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.*.baz', 'foo.bar.baz'))
    })
    it("should return true if wildcard is 'foo.*.baz' and value is foo.*.baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.*.baz', 'foo.*.baz'))
    })
    it("should return true if wildcard is '*.*.baz' and value is foo.bar.baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*.*.baz', 'foo.bar.baz'))
    })
    it("should return true if wildcard is '*.bar.*' and value is foo.bar.baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*.bar.*', 'foo.bar.baz'))
    })
    it("should return true if wildcard is '*.bar|baz.*' and value is foo.bar.baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*.bar|baz.*', 'foo.bar.baz'))
    })
    it("should return true if wildcard is '*.bar|baz.*' and value is foo.baz.bar", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*.bar|baz.*', 'foo.baz.bar'))
    })
    it("should return true if wildcard is '*.bar|.*' and value is foo..bar", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('*.bar|.*', 'foo..bar'))
    })
    it("should return true if wildcard is 'foo.*.baz' and value is foo..baz", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.*.baz', 'foo..baz'))
    })
    it("should return true if wildcard is 'foo.bar' and value is foo.bar", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.bar', 'foo.bar'))
    })
    it("should return true if wildcard is 'foo.bar.#' and value is foo.bar.baz.bam", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.bar.#', 'foo.bar.baz.bam'))
    })
    it("should return true if wildcard is 'foo.bar.#' and value is foo.bar.baz.bam", () => {
        expect(true).is.equal(WildcardMatcher.isMatch('foo.bar.#', 'foo.bar'))
    })
})

describe('WildcardMatcher.isMatch falsie wildcards test', () => {
    it("should return false if wildcard is '*' and value is foo.bar", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('*', 'foo.bar'))
    })
    it("should return false if wildcard is '*.*.*' and value is foo.bar", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('*.*.*', 'foo.bar'))
    })
    it("should return false if wildcard is 'foo.*.baz' and value is foo.bar", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('foo.*.baz', 'foo.bar'))
    })
    it("should return false if wildcard is '*.*.baz' and value is foo.bar", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('*.*.baz', 'foo.bar'))
    })
    it("should return false if wildcard is '*.bar.ban.*' and value is foo.bar.bar.baz", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('*.bar.ban.*', 'foo.bar.bar.baz'))
    })
    it("should return false if wildcard is 'foo.*.baz' and value is foo.bar.baz.ban", () => {
        expect(false).is.equal(WildcardMatcher.isMatch('foo.*.baz', 'foo.bar.baz.ban'))
    })
})

describe('WildcardMatcher.matchFilter wildcards test', () => {
    it("should return 1 wildcard without #", () => {
        let wildcards = ['foo.*.baz', '*', '*.*.*.*']
        expect(1).is.equal(WildcardMatcher.matchFilter(wildcards, 'foo.bar.baz').length)
    })
    it("should return 2 wildcards without #", () => {
        let wildcards = ['foo.*.baz', '*', '*.*.*']
        expect(2).is.equal(WildcardMatcher.matchFilter(wildcards, 'foo.bar.baz').length)
    })
    it("should return 3 wildcards with #", () => {
        let wildcards = ['foo.*.baz', '*', '*.*.*', 'foo.#']
        expect(3).is.equal(WildcardMatcher.matchFilter(wildcards, 'foo.bar.baz').length)
    })
    it("should return 4 wildcards with #", () => {
        let wildcards = ['foo.*.baz', '*', '*.*.*', 'foo.#','foo.baz.*', 'foo.bar.baz']
        expect(4).is.equal(WildcardMatcher.matchFilter(wildcards, 'foo.bar.baz').length)
    })
})

