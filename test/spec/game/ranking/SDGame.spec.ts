import { Player, PlayerType } from '../../../../src/game/Player'
import { SDGame, SDGameConfig } from '../../../../src/game/SDGame'
import { EventInterface, PullrequestMerged, BugFixed, MaintFixed, EventFactory, EventType } from '../../../../src/game/event'
import { expect } from 'chai'
import 'mocha'

describe('SD Game pull table pullrequest.merged events', () => {
    it('Correct sum of pullrequest.merged events', () => {
        let pullRequestMergedEvent = new PullrequestMerged({
            id: '1',
            name: 'pullrequest.merged',
            createdAt: new Date('2018-01-02'),
            version: 1,
            data: {
                id: '1',
                createdBy: 'JohnConnor',
                mergedBy: 'JohnConnor',
                mergedAt: new Date('2018-01-02'),
                participants: [{
                    username: 'BruceWayne',
                    approved: false,
                    role: 'participant'
                }]
            }
        })

        let pullRequestMergedEvent2 = new PullrequestMerged({
            id: '2',
            name: 'pullrequest.merged',
            createdAt: new Date('2018-01-02'),
            version: 1,
            data: {
                id: '2',
                createdBy: 'PeterParker',
                mergedBy: 'PeterParker',
                mergedAt: new Date('2018-01-02'),
                participants: [
                    {
                        username: 'BruceWayne',
                        approved: false,
                        role: 'participant'
                    },
                    {
                        username: 'KlarkKent',
                        approved: false,
                        role: 'participant'
                    },
                    {
                        username: 'JohnConnor',
                        approved: false,
                        role: 'participant'
                    }
                ]
            }
        }
        )

        let pullRequestMergedEvent3 = new PullrequestMerged({
            id: '3',
            name: 'pullrequest.merged',
            createdAt: new Date('2018-01-02'),
            version: 1,
            data: {
                id: '3',
                createdBy: 'PeterParker',
                mergedBy: 'PeterParker',
                mergedAt: new Date('2018-01-02'),
                participants: [{
                    username: 'JohnConnor',
                    approved: false,
                    role: 'participant'
                },
                {
                    username: 'BruceWayne',
                    approved: false,
                    role: 'participant'
                }]
            }
        })

        let players: Array<Player> = createPlayers()
        let rankingGameConfig: SDGameConfig = {
            startDate: new Date('2018-01-01'),
            endDate: new Date('2018-03-01'),
            id: '1',
            players: players,
            events: [pullRequestMergedEvent, pullRequestMergedEvent2, pullRequestMergedEvent3]
        }
        let rankingGame = new SDGame(rankingGameConfig)
        let table = rankingGame.getRanking()

        expect('JohnConnor').is.equal(table[0].playerId)
        expect('BruceWayne').is.equal(table[1].playerId)
        expect('PeterParker').is.equal(table[2].playerId)

    })
})

describe('SD Game events pullrequest.merged bug.fixed and maint.fixed', () => {
    it('JohnConnor has first place in ranking, PeterParker second and BruceWayne third', () => {
        let players = createPlayers()
        let events = createEvents()
        let thirtyDaysMiliseconds = 1000*3600*24*30;
        let startDate = new Date(new Date().getTime() - thirtyDaysMiliseconds);
        let endDate = new Date(new Date().getTime() + thirtyDaysMiliseconds);
        
        let SDGameConfig: SDGameConfig = {
            startDate: startDate,
            endDate: endDate,
            id: '1',
            players: players,
            events: events
        }

        let sdGame = new SDGame(SDGameConfig)
        let ranking = sdGame.getRanking()
        let challenges = sdGame.getChallenges()
        
        expect('JohnConnor').is.equal(ranking[0].playerId)
        expect('PeterParker').is.equal(ranking[1].playerId)
        expect('BruceWayne').is.equal(ranking[2].playerId)
        expect(2).is.equal(challenges.JohnConnor.maint.level)
        expect(2).is.equal(challenges.PeterParker.maint.level)
    })
})

function createEvents(): Array<EventInterface> {
    let events = []
    events.push(EventFactory.create('pullRequest.merged', {
        id: '1',
        createdBy: 'JohnConnor',
        mergedBy: 'JohnConnor',
        mergedAt: new Date('2018-01-02'),
        participants: [{
            username: 'BruceWayne',
            approved: false,
            role: 'participant'
        }]
    }
    ))

    events.push(EventFactory.create('pullRequest.merged', {
        id: '1',
        createdBy: 'JohnConnor',
        mergedBy: 'JohnConnor',
        mergedAt: new Date('2018-01-02'),
        participants: [{
            username: 'BruceWayne',
            approved: false,
            role: 'participant'
        }]
    }
    ))

    events.push(EventFactory.create('pullRequest.merged', {
        id: '1',
        createdBy: 'PeterParker',
        mergedBy: 'JohnConnor',
        mergedAt: new Date('2018-01-02'),
        participants: [{
            username: 'JohnConnor',
            approved: false,
            role: 'participant'
        }]
    }))

    events.push(EventFactory.create('bug.fixed', {
        id: '1',
        fixedBy: 'PeterParker'
    }))

    events.push(EventFactory.create('bug.fixed', {
        id: '1',
        fixedBy: 'JohnConnor'
    }))

    events.push(EventFactory.create('bug.fixed', {
        id: '1',
        fixedBy: 'BruceWayne'
    }))

    events.push(EventFactory.create('bug.fixed', {
        id: '1',
        fixedBy: 'JohnConnor'
    }))

    events.push(EventFactory.create('maint.fixed', {
        id: '1',
        fixedBy: ['JohnConnor', 'PeterParker']
    }))

    events.push(EventFactory.create('maint.fixed', {
        id: '1',
        fixedBy: ['JohnConnor', 'PeterParker']
    }))

    return events
}

function createPlayers(): Array<Player> {
    let names = ['JohnConnor', 'PeterParker', 'BruceWayne', 'KlarkKent']
    let players: Array<Player> = []
    players = names.map((name: string): Player => {
        return new Player({
            id: name,
            name: name,
            bitbucketLogin: name,
            jiraLogin: name,
        } as PlayerType)
    })
    return players
}