// let eventFromBb = {
//     "merge_commit": {
//         "hash": "0657685c4934",
//         "links": {
//             "self": {
//                 "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/commit/0657685c4934"
//             }
//         }
//     },
//     "description": "Możliwość wybrania powiatu i gminy w Danych kontaktowych w edycji użytkownika\r\nfront\r\nFCC-10030",
//     "links": {
//         "decline": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/decline"
//         },
//         "commits": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/commits"
//         },
//         "self": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574"
//         },
//         "comments": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/comments"
//         },
//         "merge": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/merge"
//         },
//         "html": {
//             "href": "https://bitbucket.org/ftdev/fcc/pull-requests/9574"
//         },
//         "activity": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/activity"
//         },
//         "diff": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/diff"
//         },
//         "approve": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/approve"
//         },
//         "statuses": {
//             "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/pullrequests/9574/statuses"
//         }
//     },
//     "title": "Możliwość wybrania powiatu i gminy w Danych kontaktowych w edycji użytkownika",
//     "close_source_branch": true,
//     "reviewers": [],
//     "destination": {
//         "commit": {
//             "hash": "fc5e6e52e775",
//             "links": {
//                 "self": {
//                     "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/commit/fc5e6e52e775"
//                 }
//             }
//         },
//         "repository": {
//             "links": {
//                 "self": {
//                     "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc"
//                 },
//                 "html": {
//                     "href": "https://bitbucket.org/ftdev/fcc"
//                 },
//                 "avatar": {
//                     "href": "https://bitbucket.org/ftdev/fcc/avatar/32/"
//                 }
//             },
//             "type": "repository",
//             "name": "fcc",
//             "full_name": "ftdev/fcc",
//             "uuid": "{e5db5ce8-d3fc-4201-8f8f-37c273d2ca5e}"
//         },
//         "branch": {
//             "name": "develop"
//         }
//     },
//     "state": "MERGED",
//     "closed_by": {
//         "username": "kplewka",
//         "display_name": "Kamil Plewka",
//         "type": "user",
//         "uuid": "{0996285a-039d-4048-8030-d73b165537b1}",
//         "links": {
//             "self": {
//                 "href": "https://api.bitbucket.org/2.0/users/kplewka"
//             },
//             "html": {
//                 "href": "https://bitbucket.org/kplewka/"
//             },
//             "avatar": {
//                 "href": "https://bitbucket.org/account/kplewka/avatar/32/"
//             }
//         }
//     },
//     "summary": {
//         "raw": "Możliwość wybrania powiatu i gminy w Danych kontaktowych w edycji użytkownika\r\nfront\r\nFCC-10030",
//         "markup": "markdown",
//         "html": "<p>Możliwość wybrania powiatu i gminy w Danych kontaktowych w edycji użytkownika\nfront\n<a  id=\"detail-summary-web-item-57LGM\" rel=\"nofollow\"  href=\"https://focustelecom.atlassian.net/browse/FCC-10030\"   target=\"_blank\"  data-module-key=\"dvcs-connector-issue-key-linker\" data-plugin-key=\"jira-bitbucket-connector-plugin.00062bb6-0950-3a79-a5a9-41a368df4dd2\" data-target-type=\"dialog\" data-target-options=\"{&quot;chrome&quot;: false, &quot;externalTargetUrl&quot;: &quot;/browse/{match.1}&quot;, &quot;productContext&quot;: {&quot;encContext&quot;: &quot;gAAAAABaaNt4HIzi4FZU6o-JXBhP_9fWK9bvbflYTLMSeuyG9sn4Sh5B62UwlQP0ob6n-mZFuQV05sEv0Fk9oPOyLlJvpn3BTMEoRzM5_Ji6hNfWATf_cqjxaCfsavbysC03BIOU42ePYcMFNWC_mEoUCVCit5xbq0pOmAKKfHTtNfLqzId6EAdjlhSn8C8W1uir0Sx5osIxPkhaxzeUt8x5YXe7VXmKL0RHpOje0MJnEpqE8CsIshy-MYYBljDqJnzkIPiKs-7_jgTFtqPVKIX-A1jtXTSWvkHfQgoTu-3SqFNFsa7xwfE1rnLxRLe0YYyTsSdZIZRxZee1ipgBKVzxe4M0nVQ1zcQU3AHa1F7_-J3LKJp-TIc8iAX_t0X5Jff329fIPl2_6XomUjzcT4atDwUmdtTjLbl9Eo1ZKeat-R3luGbroIgxp4P-2N-xo-4OGXKzA6RrawWEbRmjY7VMKtPnTgHqwVFXiVQA9pJkprylz-4TsAavpbV55McEBGJVJ0BDTsO_sj4m3k_uOk3G6fvkoN91F7sc0DdPl7KU-979f-KBw2cxQlW5QD0YNkyn6cXVGHtF79OpDfr4zdBIJLa_EjJbeuuGED-jTQ_xoINDPeTMSKYLYPQi8iI02m7-TJewRdoddIWJLOX_FK0lzL8uOCcs8DryD3DEq5PgdIdfKRiNNOtobNm27BjLKc7VECGuxTBqux_4VnTikikvQLWXhsrakFixKSX_gVd1ikbsVCLYEDELalzoadVj5ZyjVIAH5pft8dy7FW9ALkdUCprc-HxeGw==&quot;}, &quot;size&quot;: &quot;maximum&quot;}\" data-ct=\"bitbucket.connect.linkers.href.event.click\" data-ct-data='{\"linker_key\": \"dvcs-connector-issue-key-linker\", \"target_type\": \"dialog\"}' class=\"detail-summary--item--link  ap-dialog aui-button aui-button-text\" > <span class=\"aui-nav-item-label\">FCC-10030</span>  </a></p>"
//     },
//     "source": {
//         "commit": {
//             "hash": "7299ffed0ee2",
//             "links": {
//                 "self": {
//                     "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc/commit/7299ffed0ee2"
//                 }
//             }
//         },
//         "repository": {
//             "links": {
//                 "self": {
//                     "href": "https://api.bitbucket.org/2.0/repositories/ftdev/fcc"
//                 },
//                 "html": {
//                     "href": "https://bitbucket.org/ftdev/fcc"
//                 },
//                 "avatar": {
//                     "href": "https://bitbucket.org/ftdev/fcc/avatar/32/"
//                 }
//             },
//             "type": "repository",
//             "name": "fcc",
//             "full_name": "ftdev/fcc",
//             "uuid": "{e5db5ce8-d3fc-4201-8f8f-37c273d2ca5e}"
//         },
//         "branch": {
//             "name": "fcc-10030-front"
//         }
//     },
//     "comment_count": 4,
//     "author": {
//         "username": "kplewka",
//         "display_name": "Kamil Plewka",
//         "type": "user",
//         "uuid": "{0996285a-039d-4048-8030-d73b165537b1}",
//         "links": {
//             "self": {
//                 "href": "https://api.bitbucket.org/2.0/users/kplewka"
//             },
//             "html": {
//                 "href": "https://bitbucket.org/kplewka/"
//             },
//             "avatar": {
//                 "href": "https://bitbucket.org/account/kplewka/avatar/32/"
//             }
//         }
//     },
//     "created_on": "2017-12-04T14:03:21.542847+00:00",
//     "participants": [
//         {
//             "role": "PARTICIPANT",
//             "participated_on": "2017-12-05T07:23:08.317481+00:00",
//             "type": "participant",
//             "approved": false,
//             "user": {
//                 "username": "lkrawczyk101",
//                 "display_name": "Łukasz Krawczyk",
//                 "type": "user",
//                 "uuid": "{daa42f3b-0c40-49ee-a186-45b6f91d39b6}",
//                 "links": {
//                     "self": {
//                         "href": "https://api.bitbucket.org/2.0/users/lkrawczyk101"
//                     },
//                     "html": {
//                         "href": "https://bitbucket.org/lkrawczyk101/"
//                     },
//                     "avatar": {
//                         "href": "https://bitbucket.org/account/lkrawczyk101/avatar/32/"
//                     }
//                 }
//             }
//         },
//         {
//             "role": "PARTICIPANT",
//             "participated_on": "2017-12-04T15:17:23.903182+00:00",
//             "type": "participant",
//             "approved": true,
//             "user": {
//                 "username": "jleszczynski",
//                 "display_name": "Jakub Leszczyński",
//                 "type": "user",
//                 "uuid": "{f1db529f-448c-45f1-8ef9-1f9f131bcbf1}",
//                 "links": {
//                     "self": {
//                         "href": "https://api.bitbucket.org/2.0/users/jleszczynski"
//                     },
//                     "html": {
//                         "href": "https://bitbucket.org/jleszczynski/"
//                     },
//                     "avatar": {
//                         "href": "https://bitbucket.org/account/jleszczynski/avatar/32/"
//                     }
//                 }
//             }
//         },
//         {
//             "role": "PARTICIPANT",
//             "participated_on": "2017-12-05T08:11:46.329499+00:00",
//             "type": "participant",
//             "approved": true,
//             "user": {
//                 "username": "dcelmer",
//                 "display_name": "Damian Celmer",
//                 "type": "user",
//                 "uuid": "{6555e300-915a-4dcd-be4d-bc6ae3230767}",
//                 "links": {
//                     "self": {
//                         "href": "https://api.bitbucket.org/2.0/users/dcelmer"
//                     },
//                     "html": {
//                         "href": "https://bitbucket.org/dcelmer/"
//                     },
//                     "avatar": {
//                         "href": "https://bitbucket.org/account/dcelmer/avatar/32/"
//                     }
//                 }
//             }
//         },
//         {
//             "role": "PARTICIPANT",
//             "participated_on": "2017-12-04T15:08:34.967167+00:00",
//             "type": "participant",
//             "approved": false,
//             "user": {
//                 "username": "pszejna28",
//                 "display_name": "Przemysław Szejna",
//                 "type": "user",
//                 "uuid": "{bc139bae-1aa1-4d81-bfd4-94a238e47d7d}",
//                 "links": {
//                     "self": {
//                         "href": "https://api.bitbucket.org/2.0/users/pszejna28"
//                     },
//                     "html": {
//                         "href": "https://bitbucket.org/pszejna28/"
//                     },
//                     "avatar": {
//                         "href": "https://bitbucket.org/account/pszejna28/avatar/32/"
//                     }
//                 }
//             }
//         },
//         {
//             "role": "PARTICIPANT",
//             "participated_on": "2017-12-05T07:55:35.621103+00:00",
//             "type": "participant",
//             "approved": false,
//             "user": {
//                 "username": "kplewka",
//                 "display_name": "Kamil Plewka",
//                 "type": "user",
//                 "uuid": "{0996285a-039d-4048-8030-d73b165537b1}",
//                 "links": {
//                     "self": {
//                         "href": "https://api.bitbucket.org/2.0/users/kplewka"
//                     },
//                     "html": {
//                         "href": "https://bitbucket.org/kplewka/"
//                     },
//                     "avatar": {
//                         "href": "https://bitbucket.org/account/kplewka/avatar/32/"
//                     }
//                 }
//             }
//         }
//     ],
//     "reason": "",
//     "updated_on": "2017-12-05T08:12:57.713544+00:00",
//     "type": "pullrequest",
//     "id": 9574,
//     "task_count": 0
// }