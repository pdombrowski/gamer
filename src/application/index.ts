import { App } from './app'
import config from './config/config'

(async () => {
    const app = new App(config)
    try {
        await app.init()
        process.exit()
    } catch (e) {
        console.error(e)
        process.exit()
    }
})()