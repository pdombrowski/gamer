import {RepositoryManager} from '../repository'
import {DbClient} from "../mongo";
import {EmitterInterface} from "../emitter/src/EmitterInterface";
import {Emitter} from "../emitter";
import {GameManager} from "../game/GameManager";

export class App
{
    private repositoryManager: RepositoryManager;
    private dbClient: DbClient;
    private config: any;
    private emitter: EmitterInterface;
    private gameManager: GameManager;

    constructor(config: any) {
        this.config = config
    }
    public async init(): Promise<void> {
        await this.initDbClient();
        this.initRepositoryManager();
        this.initEmitter();
        await this.initGameManager();
        this.initHarvesters();
        return
    }

    private async initDbClient(): Promise<void> {
        this.dbClient = new DbClient();
        await this.dbClient.connect({
            host: this.config.db.host,
            dbName: this.config.db.dbName
        })
    }

    private initRepositoryManager(): void {
        this.repositoryManager = new RepositoryManager();
        this.repositoryManager.setDb(this.dbClient.getDb())
    }

    private initEmitter(): void {
        this.emitter = Emitter.getInstance(this.config.emitter.type);
    }

    private async initGameManager(): Promise<void> {
        const games = await this.repositoryManager.getRepository('GamesRepository').getGames();
        this.gameManager = new GameManager(games, this.emitter);
    }

    private initHarvesters(): void {
        // @TODO implementacja harvesterów
    }
}