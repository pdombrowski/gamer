import * as express from 'express';
import * as path from 'path';

const app = express();

const publicPath = path.join(__dirname, '..', 'frontend/dist');

app.use(express.static(publicPath))

app.set('port', process.env.PORT || 3000);

export default app;