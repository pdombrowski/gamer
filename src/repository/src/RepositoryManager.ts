import { 
    RepositoryInterface,
    GamesRepositoryInterface,
    PlayersRepositoryInterface,
    EventsRepositoryInterface 
} from './interface/RepositoryInterface'
import {
    GamesRepository,
    PlayersRepository,
    EventsRepository
} from './repository'
import {Db} from 'mongodb'

type repositoryHashType = { [repositoryName: string]: RepositoryInterface }

export class RepositoryManager {
    private repositoryHash = {} as repositoryHashType
    private db: Db

    public getRepository(repositoryName: 'GamesRepository'): GamesRepositoryInterface
    public getRepository(repositoryName: 'PlayersRepository'): PlayersRepositoryInterface
    public getRepository(repositoryName: 'EventsRepository'): EventsRepositoryInterface
    public getRepository(repositoryName: string): RepositoryInterface {
        if (!this.repositoryHash[repositoryName]) {
            this.repositoryHash[repositoryName] = this.createRepository(repositoryName)
        }
        return this.repositoryHash[repositoryName]
    }

    public setDb(db: Db) {
        this.db = db
    }

    public getDb(): Db {
        return this.db
    }

    private createRepository(repositoryName: string): RepositoryInterface {
        switch (repositoryName) {
            case 'GamesRepository':
                return new GamesRepository(this)
            case 'PlayersRepository':
                return new PlayersRepository(this)
            case 'EventsRepository':
                return new EventsRepository(this)
            default:
                throw new Error('nie ma repozytorium ' + repositoryName)
        }
    }
}

