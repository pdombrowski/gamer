import { SDGame } from '../../../game/SDGame'
import { EventInterface } from '../../../game/event/Event'
import { EntityInterface } from '../../../common/interface/EntityInterface'
import { Player } from '../../../game/Player'

export interface RepositoryInterface {
    save(entityInterface: EntityInterface): Promise<void>
}
export interface GamesRepositoryInterface extends RepositoryInterface {
    getGames(): Promise<Array<SDGame>>
    getGame(id: string): Promise<SDGame>
}
export interface PlayersRepositoryInterface extends RepositoryInterface {
    getPlayers(): Promise<Array<Player>>
    getPlayer(id: string): Promise<Player>
}
export interface EventsRepositoryInterface extends RepositoryInterface {
    getEvents(startDate: Date, endDate: Date): Promise<Array<EventInterface>>
}