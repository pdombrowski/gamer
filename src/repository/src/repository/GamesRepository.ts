import { RepositoryInterface } from '../interface/RepositoryInterface'
import { RepositoryManager } from '../RepositoryManager'
import { SDGame } from '../../../game/SDGame'
import {GameInterface} from "../../../game/GameInterface";

export class GamesRepository implements RepositoryInterface {
    private games: Array<GameInterface> = [];
    private repositoryManager: RepositoryManager;
    private collectionName: string;

    constructor(repositoryManager: RepositoryManager) {
        this.collectionName = 'games';
        this.repositoryManager = repositoryManager;
    }

    async save(game: GameInterface) {

    }

    async getGames(): Promise<Array<GameInterface>> {
        if (this.games.length === 0) {
            this.games = await this.fetchGames();
        }
        return this.games;
    }

    async getGame(id: string): Promise<GameInterface> {
        let games = await this.getGames();
        games.filter((game: GameInterface) => {
            return game.getId() === id;
        });
        return games.length === 1 ? games[0] : null
    }

    private async fetchGames(): Promise<Array<GameInterface>> {
        let players = await this.repositoryManager.getRepository('PlayersRepository').getPlayers();
        let events = await this.repositoryManager.getRepository('EventsRepository').getEvents(new Date(), new Date());
        let thirtyDaysMiliseconds = 1000 * 3600 * 24 * 30;
        let startDate = new Date(new Date().getTime() - thirtyDaysMiliseconds);
        let endDate = new Date(new Date().getTime() + thirtyDaysMiliseconds);

        let SDGameConfig = {
            startDate: startDate,
            endDate: endDate,
            id: '1',
            players: players,
            events: events
        };


        let SDGameConfig2 = {
            startDate: startDate,
            endDate: endDate,
            id: '2',
            players: players,
            events: events
        };

        return [
            new SDGame(SDGameConfig),
            new SDGame(SDGameConfig2)
        ]
    }
}