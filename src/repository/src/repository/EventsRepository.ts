import { RepositoryInterface } from '../interface/RepositoryInterface'
import { EventInterface, EventType } from '../../../game/event/Event'
import { EventFactory } from '../../../game/event/EventFactory'
import { RepositoryManager } from '../RepositoryManager'

export class EventsRepository implements RepositoryInterface {
    private repositoryManager: RepositoryManager
    private collectionName: string
    constructor(repositoryManager: RepositoryManager) {
        this.collectionName = 'events'
        this.repositoryManager = repositoryManager
    }

    async save(event: EventInterface) {
        let eventExists = await this.getById(event.getId())
        console.log(eventExists)
        if (!eventExists) {
            await this.getCollection().insertOne(this.serialize(event))
        } else {
            this.getCollection().updateOne({ id: eventExists.getId() }, this.serialize(eventExists))
        }
    }

    async getEvents(startDate: Date, endDate: Date): Promise<Array<EventInterface>> {
        let eventTypes = await this.getCollection().find({ createdAt: { $gte: startDate, $lte: endDate } }).toArray()
        return eventTypes.map(this.wakeup.bind(this))
    }

    private async getById(id: string): Promise<EventInterface> {
        let eventType: EventType = await this.getCollection().findOne({ id: id })
        if (eventType) {
            return this.wakeup(eventType)
        }
        return null
    }

    private getCollection() {
        return this.repositoryManager.getDb().collection(this.collectionName)
    }

    private serialize(event: EventInterface) {
        let exportedEvent = event.export()
        return exportedEvent
    }

    private wakeup(event: EventType) {
        return EventFactory.rebuild(event)
    }
}