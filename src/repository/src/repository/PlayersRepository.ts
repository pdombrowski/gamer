import { RepositoryInterface } from '../interface/RepositoryInterface'
import { Player, PlayerType } from '../../../game/Player'
import { RepositoryManager } from '../RepositoryManager'

export class PlayersRepository implements RepositoryInterface {
    private players: Array<Player> = []
    private repositoryManager: RepositoryManager
    private collectionName: string

    constructor(repositoryManager: RepositoryManager) {
        this.collectionName = 'players'
        this.repositoryManager = repositoryManager
    }

    async save(entity: Player) {
        let player = await this.getById(entity.getId())
        if (!player) {
            await this.getCollection().insertOne(this.serialize(entity))
        } else {
            this.getCollection().updateOne({ id: player.getId() }, this.serialize(entity))
        }
    }

    async getPlayers(): Promise<Array<Player>> {
        let playersTypes = await this.getCollection().find({}).toArray()
        return playersTypes.map(this.wakeup.bind(this))
    }

    async getPlayer(id: string): Promise<Player> {
        return await this.getById(id)
    }

    private async getById(id: string): Promise<Player> {
        let playerType: PlayerType = await this.getCollection().findOne({ id: id })
        if (playerType) {
            return this.wakeup(playerType)
        }
        return null
    }

    private serialize(player: Player) {
        let exportedPlayer = player.export()
        return exportedPlayer
    }

    private wakeup(playerType: PlayerType) {
        return new Player(playerType)
    }

    private getCollection() {
        return this.repositoryManager.getDb().collection(this.collectionName)
    }
}