import {ExportableInterface} from './ExportableInterface'

export interface EntityInterface extends ExportableInterface {
    getId(): string
}