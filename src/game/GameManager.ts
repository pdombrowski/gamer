import {EventInterface} from "./event";
import {Emitter} from "../emitter";
import {GameInterface} from "./GameInterface";
import {EmitterInterface} from "../emitter/src/EmitterInterface";

export class GameManager
{
    private games: Array<GameInterface> = [];
    private emitter: EmitterInterface;

    public constructor(games: Array<GameInterface>, emitter: EmitterInterface = null) {
        this.emitter = emitter;
        this.games = games;
        this.games.forEach((game: GameInterface) => {
            console.log('Zarejestrowane listenery dla gry: ' + game.getId());
            this.registerListeners(game);
        });
    }

    private registerListeners(game: GameInterface) : void {
        if (this.emitter) {
            this.emitter.on(Emitter.EVENT_EVENT_NEW, (event: EventInterface) => {
                game.addEvent(event);
            });
            this.emitter.on(Emitter.EVENT_GAME_FINISHED, (gameFinished: GameInterface) => {
                if (gameFinished.getId() === game.getId() && !game.isFinished()) {
                    // #TODO implemenacja zakończenia gry
                }
            })
        }
    }
}