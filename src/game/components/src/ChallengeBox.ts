import { Challenge } from './Challenge'

type ChallengeConfig = {
    name: string,
    goals: Array<number>
}

type ChallengeSetConstruct = {
    players: Array<string>,
    challengesConfig: Array<ChallengeConfig>
}

type ChallengeSet = { [challengeName: string]: Challenge }

type ChallengeHash = {
    [playerId: string]: ChallengeSet
}

type RawChallenges = {
    [playerId: string]: {
        [challengeName: string]: {
            points: number,
            level: number,
            goal: number
        }
    }
}

export class ChallengeBox {
    private challengeHash: ChallengeHash
    constructor(challengeSetConstruct: ChallengeSetConstruct) {
        this.challengeHash = this.createChallengeHash(challengeSetConstruct.players, challengeSetConstruct.challengesConfig)
    }

    addChallengePoints(player: string, challengeName: string, points: number) {
        let playerChallenge = this.getPlayerChallenge(player, challengeName)
        if (playerChallenge) {
            playerChallenge.addPoints(points)
        }
    }

    getRawChallenges(): RawChallenges {
        let rawChallenges = {} as RawChallenges
        for (var playerId in this.challengeHash) {
            let challengeSet = this.challengeHash[playerId]
            rawChallenges[playerId] = {}
            for (var challengeName in challengeSet) {
                let challenge = challengeSet[challengeName]
                rawChallenges[playerId][challengeName] = {
                    points: challenge.getPoints(),
                    level: challenge.getLevel(),
                    goal: challenge.getGoal()
                }
            }
        }
        return rawChallenges
    }

    private getPlayerChallenges(player: string): ChallengeSet {
        return this.challengeHash[player] || null
    }

    private getPlayerChallenge(player: string, challengeName: string): Challenge {
        let playerChallenges = this.getPlayerChallenges(player)
        return playerChallenges ? playerChallenges[challengeName] : null
    }

    private createChallengeHash(players: Array<string>, challengesConfig: Array<ChallengeConfig>): ChallengeHash {
        let challenges = {} as ChallengeHash

        players.forEach((player) => {
            challenges[player] = this.createChallengeSet(challengesConfig)
        })

        return challenges;
    }

    private createChallengeSet(challengesConfig: Array<ChallengeConfig>) {
        let challengeSet = {} as ChallengeSet
        challengesConfig.forEach((challengeConfig) => {
            challengeSet[challengeConfig.name] = new Challenge({
                name: challengeConfig.name,
                goals: challengeConfig.goals,
                points: 0
            })
        })
        return challengeSet
    }
}