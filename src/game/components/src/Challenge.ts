type ChallengeConstruct = {
    readonly name: string
    readonly points: number
    readonly goals: Array<number>
}

export class Challenge {
    private name: string
    private points: number
    private goals: Array<number>

    constructor(challengeConfig: ChallengeConstruct) {
        this.name = challengeConfig.name
        this.points = challengeConfig.points
        this.goals = challengeConfig.goals.sort()
    }

    getName(): string {
        return this.name
    }

    getLevel(): number {
        let level = this.points >= this.getLastGoal() ? this.goals.length : this.goals.indexOf(this.getGoal())
        return level + 1
    }

    getGoal(): number {
        let val = this.goals.find((goal) => {
            return goal > this.points
        }) || this.getLastGoal()
        
        return val
    }

    getPoints(): number {
        return this.points
    }

    addPoints(points: number): void {
        this.points += points
    }

    private getLastGoal() {
        return this.goals[this.goals.length -1]
    }
}