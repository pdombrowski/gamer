type TableRow = {
    position: number,
    playerId: string,
    points: PointsSet
}

type PointsSet = { [pointsName: string]: number }

type RankingHash = {
    [playerId: string]: PointsSet
}

type PointsConfigHash = {
    [pointsName: string]: PointsConfig
}

type PointsConfig = {
    multipler: number,
    display: string
}

interface DisplayPointsPolicy {
    display(value: number): number
}

class DisplayFactory {
    public static create(name: string, params: PointsConfig) {
        switch (name) {
            case 'raw':
                return new RawDisplay()
            case 'calculated':
                return new CalculatedDisplay(params.multipler)
        }
    }
}

class RawDisplay implements DisplayPointsPolicy {
    display(value: number): number {
        return value
    }
}

class CalculatedDisplay implements DisplayPointsPolicy {
    private multipler: number
    constructor(multipler: number) {
        this.multipler = multipler
    }

    display(value: number): number {
        return Math.floor(this.multipler * value)
    }
}

type TableConstruct = {
    players: Array<string>
    points: PointsConfigHash
}

export class Table {
    private rankingHash: RankingHash
    private config: TableConstruct

    constructor(construct: TableConstruct) {
        this.config = JSON.parse(JSON.stringify(construct))
        this.rankingHash = this.createRankingHash()
    }

    addPoints(type: string, value: number, playerId: string) {
        if (!this.hasPointType(type)) {
            throw 'Nie obsługuję punktów typu ' + type
        }
        if (!this.hasPlayer(playerId)) {
            throw 'Nie mam gracza o id ' + playerId
        }
        this.rankingHash[playerId][type] += value
    }

    getTable(): Array<TableRow> {
        let table = [] as Array<TableRow>
        for (var playerId in this.rankingHash) {
            let displayPointsSet = this.getDisplayPointsSet(this.rankingHash[playerId])
            let tableRow = {
                position: 0,
                playerId: playerId,
                points: displayPointsSet
            }
            table.push(tableRow)
        }
        let sortedTable = table.sort(this.sort)
        let counter = 1;
        sortedTable.forEach((rankingRow: TableRow) => {
            rankingRow.position = counter++
        })
        return table
    }

    private sort(rankingRow1: TableRow, rankingRow2: TableRow): number {
        return rankingRow2.points.total - rankingRow1.points.total
    }

    private createRankingHash(): RankingHash {
        let rankingHash = {} as RankingHash
        this.config.players.forEach((player) => {
            rankingHash[player] = this.createPointsSet()
        })
        return rankingHash
    }

    private getDisplayPoints(name: string, value: number) {
        let pointsConfig = this.config.points[name]
        let pointsDisplayer = DisplayFactory.create(pointsConfig.display, pointsConfig)
        return pointsDisplayer.display(value)
    }

    private getDisplayPointsSet(pointsSet: PointsSet): PointsSet {
        let pointsSetCopy = JSON.parse(JSON.stringify(pointsSet))
        pointsSetCopy.total = 0
        for (var pointsName in pointsSet) {
            let pointsCalculator = DisplayFactory.create('calculated', this.config.points[pointsName])
            pointsSetCopy[pointsName] = this.getDisplayPoints(pointsName, pointsSetCopy[pointsName])
            pointsSetCopy.total += pointsCalculator.display(pointsSetCopy[pointsName])
        }

        return pointsSetCopy
    }

    private createPointsSet(): PointsSet {
        let pointsSet = {} as PointsSet
        for (var pointsName in this.config.points) {
            pointsSet[pointsName] = 0
        }
        return pointsSet
    }

    private hasPointType(pointType: string) {
        let supportedPointTypes = Object.keys(this.config.points)
        return !!~supportedPointTypes.indexOf(pointType)
    }

    private hasPlayer(playerId: string): boolean {
        return !!~this.config.players.indexOf(playerId)
    }
}