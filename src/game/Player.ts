import {EntityInterface} from '../common/interface/EntityInterface'

type PlayerConfig = {
    id: string,
    name: string,
    bitbucketLogin: string,
    jiraLogin: string,
}

export type PlayerType = PlayerConfig

export class Player implements EntityInterface {
    private id: string
    private name: string
    private bitbucketLogin: string
    private jiraLogin: string

    constructor(config: PlayerConfig) {
        this.id = config.id
        this.name = config.name
        this.bitbucketLogin = config.bitbucketLogin,
        this.jiraLogin = config.jiraLogin
    }

    public getId() : string {
        return this.id
    }

    public getName(): string {
        return this.name
    }

    public getBitbucketLogin(): string {
        return this.bitbucketLogin
    }

    public getJiraLogin(): string {
        return this.jiraLogin
    }

    public export(): PlayerType {
        return {
            id: this.getId(),
            name: this.getName(),
            bitbucketLogin: this.getBitbucketLogin(),
            jiraLogin: this.getJiraLogin()
        }
    }
}