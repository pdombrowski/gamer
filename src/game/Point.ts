type PointConfig = {
    readonly id: string
    readonly name: string
    readonly value: number
    readonly playerId: string
    readonly craetedAt: Date
}

export class Point {
    private id: string
    private name: string
    private value: number
    private playerId: string
    private craetedAt: Date
}