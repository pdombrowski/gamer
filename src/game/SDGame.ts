import { Player } from './Player'
import { Table, ChallengeBox } from './components'
import { EventInterface, PullrequestMerged, BugFixed, MaintFixed } from './event'
import { GameInterface } from "./GameInterface";

export type SDGameConfig = {
    id: string,
    startDate: Date,
    endDate: Date,
    players: Array<Player>,
    events: Array<EventInterface>
}

export class SDGame implements GameInterface {
    private id: string;
    private startDate: Date;
    private endDate: Date;
    private events: Array<EventInterface>;
    private players: Array<Player>;
    private table: Table;
    private challengeBox: ChallengeBox;

    constructor(config: SDGameConfig) {
        this.events = [];
        this.id = config.id;
        this.players = config.players;
        this.startDate = config.startDate;
        this.endDate = config.endDate;
        this.table = this.createTable(this.players);
        this.challengeBox = this.createChallengeBox(this.players);
        config.events.forEach(this.addEvent)
    }

    getStartDate() : Date {
        return this.startDate;
    }

    getEndDate() : Date {
        return this.endDate;
    }

    export(): SDGameConfig {
        return {
            id: this.id,
            startDate: this.startDate,
            endDate: this.endDate,
            players: this.players,
            events: this.events
        }
    }

    getRanking() {
        return this.table.getTable()
    }

    getChallenges() {
        return this.challengeBox.getRawChallenges()
    }

    isFinished(): boolean {
        return false
    }

    getId(): string {
        return this.id
    }

    addEvent(event: EventInterface): boolean {
        if (!this.isEventMemberOfRanking(event) || this.hasEvent(event)) {
            return false;
        }
        this.events.push(event)
        this.processEvent(event);
    }

    private createTable(players: Array<Player>): Table {
        return new Table({
            players: players.map((player: Player) => player.getId()),
            points: {
                maint: {
                    display: 'raw',
                    multipler: 2.5
                },
                bugfix: {
                    display: 'raw',
                    multipler: 1.5
                },
                pr: {
                    display: 'raw',
                    multipler: 0.85
                }
            }
        })
    }

    private createChallengeBox(players: Array<Player>): ChallengeBox {
        let challengeConfig = {
            players: players.map((player: Player) => {
                return player.getId()
            }),
            challengesConfig: [
                {
                    name: 'pr',
                    goals: [10, 15, 23, 28, 36, 42, 50, 55, 63]
                },
                {
                    name: 'maint',
                    goals: [1, 3, 4, 5, 7, 10, 13, 16, 19, 23, 28, 33, 38, 43]
                },
                {
                    name: 'bugfix',
                    goals: [2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
                }
            ]
        }

        return new ChallengeBox(challengeConfig)
    }

    private addChallengePoints(player: Player, challengeType: string, points: number) {
        this.challengeBox.addChallengePoints(player.getId(), challengeType, points)
    }

    private processEvent(event: EventInterface) {
        switch (event.getName().toLowerCase()) {
            case 'pullrequest.merged':
                this.processPullRequestMerged(event as PullrequestMerged)
                break;
            case 'bug.fixed':
                this.processBugFixed(event as BugFixed)
                break;
            case 'maint.fixed':
                this.processMaintFixed(event as MaintFixed)
                break;
        }
    }

    private processPullRequestMerged(pullRequestMergedEvent: PullrequestMerged) {
        let data = pullRequestMergedEvent.getData()
        let createdByPlayer = this.getPlayerByBitbucketLogin(data.createdBy);
        if (createdByPlayer) {
            this.table.addPoints('pr', 1, createdByPlayer.getId())
            this.addChallengePoints(createdByPlayer, 'pr', 1)
        }

        data.participants.forEach((participant) => {
            if (participant.username !== data.createdBy) {
                let player = this.getPlayerByBitbucketLogin(participant.username);
                if (player) {
                    this.table.addPoints('pr', 1, player.getId())
                    this.addChallengePoints(player, 'pr', 1)
                }
            }
        })
    }

    private processBugFixed(bugFixedEvent: BugFixed) {
        let data = bugFixedEvent.getData()
        let player = this.getPlayerByJiraLogin(data.fixedBy);
        if (player) {
            this.table.addPoints('bugfix', 1, player.getId())
            this.addChallengePoints(player, 'bugfix', 1)
        }
    }

    private processMaintFixed(maintFixedEvent: MaintFixed) {
        let data = maintFixedEvent.getData()
        data.fixedBy.forEach((jiraLogin) => {
            let player = this.getPlayerByJiraLogin(jiraLogin);
            if (player) {
                this.table.addPoints('maint', 1, player.getId())
                this.addChallengePoints(player, 'maint', 1)
            }
        })
    }

    private getPlayerByJiraLogin(jiraLogin: string): Player {
        return this.players.find((player) => {
            return player.getJiraLogin() === jiraLogin
        })
    }

    private getPlayerByBitbucketLogin(bitbucketLogin: string): Player {
        return this.players.find((player) => {
            return player.getBitbucketLogin() === bitbucketLogin
        })
    }

    private hasEvent(event: EventInterface) {
        return typeof this.events.find((gameEvent: EventInterface) => event.getId() === gameEvent.getId()) !== 'undefined';
    }

    private isEventSupported(event: EventInterface): boolean {
        return !!~['pullrequest.merged', 'bug.fixed', 'maint.fixed'].indexOf(event.getName())
    }

    private isEventInRankingPeriod(event: EventInterface): boolean {
        let eventCreatedAtStamp = event.getCreatedAt().getTime()
        let startStamp = this.startDate.getTime()
        let endStamp = this.endDate.getTime()
        return eventCreatedAtStamp > startStamp && eventCreatedAtStamp < endStamp
    }

    private isEventMemberOfRanking(event: EventInterface): boolean {
        return this.isEventSupported(event) && this.isEventInRankingPeriod(event)
    }
}