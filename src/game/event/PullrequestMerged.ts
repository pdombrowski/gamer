import { EventInterface, EventType } from './Event'

type Participant = {
    readonly username: string,
    readonly role: string
    readonly approved: boolean
}

type PullRequestMergedData = {
    readonly id: string,
    readonly createdBy: string,
    readonly mergedBy: string,
    readonly mergedAt: Date,
    readonly participants: Array<Participant>
}

export class PullrequestMerged implements EventInterface {
    private data : PullRequestMergedData
    private createdAt: Date
    private id: string
    private version: number

    constructor(eventType: EventType) {
        this.id = eventType.id
        this.data = Object.assign({}, eventType.data) as PullRequestMergedData
        this.createdAt = eventType.createdAt
        this.version = eventType.version
    }

    getId(): string {
        return this.id
    }

    getVersion(): number {
        return this.version
    }
    getName(): string {
        return 'pullrequest.merged'
    }
    getData(): PullRequestMergedData {
        return Object.assign({}, this.data)
    }
    getCreatedAt(): Date {
        return this.createdAt
    }
    export(): EventType {
        return {
            id: this.getId(),
            createdAt: new Date(this.getCreatedAt()),
            version: this.getVersion(),
            name: this.getName(),
            data: this.getData()
        }
    }    
}