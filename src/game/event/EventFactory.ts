import { EventInterface, EventType } from './Event'
import { PullrequestMerged } from './PullrequestMerged'
import { BugFixed } from './BugFixed'
import { MaintFixed } from './MaintFixed'

export class EventFactory {
    public static create(name: string, data: object) {
        let eventType: EventType
        switch (name.toLowerCase()) {
            case 'pullrequest.merged':
                eventType = createEventType('pullrequest.merged', data)
                break
            case 'bug.fixed':
                eventType = createEventType('bug.fixed', data)
                break
            case 'maint.fixed':
                eventType = createEventType('maint.fixed', data)
                break
        }
        return this.rebuild(eventType)
    }

    public static rebuild(eventType: EventType) {
        switch (eventType.name) {
            case 'pullrequest.merged':
                return createPullRequestMerged(eventType)
            case 'bug.fixed':
                return createBugFixed(eventType)
            case 'maint.fixed':
                return createMaintFixed(eventType)
        }
    }
}

function createEventType(name: string, data: object): EventType {
    let createData = {
        id: createId(),
        name: name,
        data: data,
        createdAt: new Date(),
        version: 1
    } as EventType
    return createData
}


function createId(): string {
    return Math.round(Math.random() * 1000000).toString()
}

function createMaintFixed(data: EventType): MaintFixed {
    return new MaintFixed(data)
}

function createPullRequestMerged(data: EventType): PullrequestMerged {
    return new PullrequestMerged(data)
}

function createBugFixed(data: EventType): BugFixed {
    return new BugFixed(data)
}