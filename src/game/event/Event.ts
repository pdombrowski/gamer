import {EntityInterface} from '../../common/interface/EntityInterface'

export interface EventInterface extends EntityInterface {
    getVersion(): number
    getName(): string
    getData(): object
    getCreatedAt(): Date
}

export type EventType = {
    readonly id: string
    readonly version: number
    readonly name: string
    readonly data: object
    readonly createdAt: Date
}