import { EventInterface, EventType } from './Event'

type BugFixedData = {
    readonly id: string,
    readonly fixedBy: string
}

export class BugFixed implements EventInterface {
    private data : BugFixedData
    private createdAt: Date
    private id: string
    private version: number

    constructor(eventType: EventType) {
        this.id = eventType.id
        this.data = Object.assign({}, eventType.data) as BugFixedData
        this.createdAt = eventType.createdAt
        this.version = eventType.version
    }

    getId(): string {
        return this.id
    }

    getVersion(): number {
        return this.version
    }
    getName(): string {
        return 'bug.fixed'
    }
    getData(): BugFixedData {
        return Object.assign({}, this.data)
    }
    getCreatedAt(): Date {
        return this.createdAt
    }
    export(): EventType {
        return {
            id: this.getId(),
            createdAt: new Date(this.getCreatedAt()),
            version: this.getVersion(),
            name: this.getName(),
            data: this.getData()
        }
    }    
}