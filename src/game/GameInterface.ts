import {EntityInterface} from "../common/interface/EntityInterface";
import {EventInterface} from "./event";
import {SDGameConfig} from "./SDGame";

export interface GameInterface extends EntityInterface {
    export() : SDGameConfig
    isFinished(): boolean
    getStartDate(): Date
    getEndDate(): Date
    addEvent(event: EventInterface) : void
}