export class WildcardMatcher {
    static isMatch(wildcard: string, value: string): boolean {
        const SEPARATOR_WILDCARD = '.'
        const SEPARATOR_OR = '|'
        let _wildcard = wildcard
        let _value = value || ''
        let _wildcardParts = _wildcard.split(SEPARATOR_WILDCARD)
        let _valueParts = _value.split(SEPARATOR_WILDCARD)
        let _isLastElementIsHash = _wildcardParts.indexOf('#') === _wildcardParts.length -1
        let _hasWildcard = !!~_wildcardParts.indexOf('*') || _isLastElementIsHash

        if(_wildcard === '#') {
            return true
        }
        
        if(!_hasWildcard) {
            return _wildcard === _value
        }

        if(!_isLastElementIsHash && _wildcardParts.length !== _valueParts.length) {
            return false
        }

        if(_isLastElementIsHash) {
            _wildcardParts.pop()
        }

        for(var i=0; i<_wildcardParts.length;i++){
            if(_wildcardParts[i] === '*') {
                continue
            }
            
            if(!~_wildcardParts[i].split('|').indexOf(_valueParts[i])) {
                return false
            }
        }

        return true
    }
    static matchFilter(wildcards: Array<string>, value: string): Array<string> {
        return wildcards.filter((wildcard) => {
            return this.isMatch(wildcard, value)
        })
    }
}
