export interface EmitterInterface
{
    emit(name: string, obj: any) : boolean;
    on(name: string,  callback: {(...args: any[]): void}) : this;
}