import {EmitterInterface} from "./EmitterInterface";
import {EventEmitter} from "events";

export class ApplicationEventEmitter extends EventEmitter implements EmitterInterface
{
    public emit(type: string, obj: any) : boolean {
        return super.emit(type, obj);
    }

    public on(type: string, callback: {(...args: any[]): void}) : this {
        return super.on(type, callback);
    }
}