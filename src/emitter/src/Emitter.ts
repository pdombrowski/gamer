import {EmitterInterface} from "./EmitterInterface";
import {ApplicationEventEmitter} from "./ApplicationEventEmitter";

interface EmitterMapInterface {
    [type: string]: EmitterInterface;
}

export class Emitter
{
    public static EVENT_EVENT_NEW = 'event.new';
    public static EVENT_GAME_FINISHED = 'game.finished';

    protected static emitter: EmitterMapInterface = {};

    protected constructor() {}

    public static getInstance(type: string = 'event') : EmitterInterface {
        if (!this.emitter[type]) {
            this.emitter[type] = this.getEmitter(type);
        }
        return this.emitter[type];
    }

    protected static getEmitter(type: string) : EmitterInterface {
        switch (type) {
            case 'event':
                return new ApplicationEventEmitter();
            default:
                throw Error(`Emitter ${type} is not defined.`);
        }
    }
}