import { MongoClient, Db } from 'mongodb'

type connectionParams = {
    dbName: string,
    host: string,
    port?: number
}

export class DbClient {
    private db: Db
    private client: MongoClient
    connect (params: connectionParams): Promise<any> {
        let port = params.port || 27017
        let host = params.host
        let dbName = params.dbName
        let url = 'mongodb://' + host + ':' + port
    
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, (err, client) => {
                if (err) {
                    reject(err)
                    return
                }
                this.client=client
                this.db = client.db(dbName)
                resolve(this.db)
            })
        })
    }
    
    disconnect():Promise<void> {
        if(this.client.isConnected) {
            return this.client.close()
        }
    }

    getDb(): Db {
        return this.db
    }
}